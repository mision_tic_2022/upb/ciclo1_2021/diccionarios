"""
===========================
Juan manuel
===========================
"""
supermercado = {
    "estadio": {
        "aseo": 1600,
        "verduras": 2600,
        "frutas": 3400
        
    },
    "belen":{
        "aseo": 2122,
        "verduras": 2422,
        "carnes": 5000,
        "frutas": 2000
        
    }
}

promedio_estadio = supermercado["estadio"]["promedio"] = (supermercado["estadio"]["aseo"] + supermercado["estadio"]["verduras"] + supermercado["estadio"]["frutas"]) / 3
print("Promedio de estadio",supermercado["estadio"])
promedio_belen = supermercado["belen"]["promedio"] = (supermercado["belen"]["aseo"] + supermercado["belen"]["verduras"] + supermercado["belen"]["frutas"]) / 4
print("Promedio de belen",supermercado["belen"])
print("Diccionario padre: ")
print(supermercado)

"""
===========================
Paulo
===========================
"""
print("===========================")
supermercado: dict = {
    "estadio": {
        "aseo": 1600000,
        "verduras": 4500000,
        "trabajadores": ["juan", "ana", "sara"]
    },
    "belen": {
        "aseo": 2000000,
        "verduras": 5000000,
        "trabajadores": ["pedro", "martha", "andres"]
    }
}
promedio = (supermercado["estadio"]["aseo"]+supermercado["estadio"]["verduras"])/2
supermercado["estadio"]["promedio_ventas"] = promedio
promedio = (supermercado["belen"]["aseo"]+supermercado["belen"]["verduras"])/2
supermercado["belen"]["promedio_ventas"] = promedio

print(supermercado)

"""
===========================
Manuela salazar
===========================
"""
print("===========================")
#Ejercicio
#Diccionario de diccionarios
supermercado: dict = {
    "estadio": {
        "aseo": 1600000,
        "verduras": 4500000,
        "trabajadores": ["juan", "ana", "sara"]
    },
    "belen": {
        "aseo": 2000000,
        "verduras": 5000000,
        "trabajadores": ["pedro", "martha", "andres"]
    }
}
#Acceder a un diccionario hijo
estadio = supermercado["estadio"]
##print(estadio)
#print( supermercado["estadio"]["aseo"] + supermercado["belen"]["aseo"] )
#Accedemos a las ventas de los productos 
ventas_aseo = estadio["aseo"]
ventas_verduras = estadio ["verduras"]
promedio_ventasEst = (ventas_aseo + ventas_verduras)/2
#Añadir nuevas clave-valor
estadio["promedio_ventas"] = promedio_ventasEst
#Belén
belen = supermercado["belen"]
ventas_aseoB = belen ["aseo"]
ventas_verdurasB = belen ["verduras"]
promedio_ventasBel = (ventas_aseoB + ventas_verdurasB)/2
belen ["promedio_ventas"] = promedio_ventasBel
print(supermercado)

