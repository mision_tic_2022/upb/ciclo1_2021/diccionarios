#Rubén
print("==============================")
print("\tRubén Antonio")
print("==============================\n")
diccionario_persona = {
    "nombre": "Juan",
    "apellido": "Herrera",
    "edad": 25,
    "estudiante": True,
    "sueldo": 10.5,
    "ventas": {
        "enero":{
            "lunes": 10,
            "martes": 22
        },
        "febrero":{
            "lunes": 12,
            "martes": 30
        }
    }
}

#Recorrer directamente los valores de un diccionario
for x in diccionario_persona.values():
    print(x)

#Recorrer los items de un diccionario
for clave, valor in diccionario_persona.items():
    print("clave: ", clave)
    print("valor: ", valor)

for clave, valor in diccionario_persona["ventas"].items():
    print("clave: ", clave)
    print("valor: ", valor)

print("Claves y valores de Enero")
for clave, valor in diccionario_persona["ventas"]["enero"].items():
    print("clave: ", clave)
    print("valor: ", valor)

print("Claves y valores de febrero")
for clave, valor in diccionario_persona["ventas"]["febrero"].items():
    print("clave: ", clave)
    print("valor: ", valor)


print("==============================")
print("\tPaulo cesar")
print("==============================\n")
#Paulo cesar
diccionario_persona = {
    "nombre": "Juan",
    "apellido": "Herrera",
    "edad": 25,
    "estudiante": True,
    "sueldo": 10.5,
    "ventas": {
        "enero":{
            "lunes": 10,
            "martes": 22,
        },
        "febrero":{
            "lunes": 12,
            "febrero": 30
        }
    }
}

#Recorrer directamente los valores de un diccionario
for x in diccionario_persona.values():
    print(x)

#Recorrer los ítems de un diccionario
for clave, valor in diccionario_persona.items():
    print("clave: ", clave)
    print("valor: ", valor)

for clave, valor in diccionario_persona["ventas"].items():
    print("clave: ", clave)
    print("valor: ", valor)

for clave, valor in diccionario_persona["ventas"]["enero"].items():
    print("clave: ", clave)
    print("valor: ", valor)

for clave, valor in diccionario_persona["ventas"]["febrero"].items():
    print("clave: ", clave)
    print("valor: ", valor)