#diccionario, estructura de un diccionario
mi_diccionario: dict = {
    "clave_1": "valor_1",
    "clave_2": "valor_2"
}
#Acceder al valor del diccionario por medio de su clave
print(mi_diccionario["clave_1"])

#Creación de un diccionario con los datos de una persona
persona: dict = {
    "nombre": "Andrés",
    "cedula": "123456",
    "edad": 20
}
print(persona)
#Actualización de valores
persona["edad"] = 25
print(persona)
#Añadir nuevas clave-valor
persona["genero"] = "M"
print(persona)

#Creación de un diccionario vacío
#universidad: dict = {}
universidad: dict = dict()
print(universidad)
universidad["nombre"] = "UPB"
print(universidad)

#Funciones de los diccionarios
print("----------Funciones de diccionarios-----------")
print(persona)
#Eliminar un item
persona.pop("genero")
print(persona)
#Obtener las claves del diccionario
claves = persona.keys()
print(claves)
#Acceder a los valores
valores = persona.values()
print(valores)
#items = persona.items()
#print(items)
#Limpiar el diccionario
persona.clear()
print(persona)

print("--------------Diccionario de diccionarios-----------")
#Diccionario de diccionarios
supermercado: dict = {
    "estadio": {
        "aseo": 1600000,
        "verduras": 4500000,
        "trabajadores": ["juan", "ana", "sara"]
    },
    "belen": {
        "aseo": 2000000,
        "verduras": 5000000,
        "trabajadores": ["pedro", "martha", "andres"]
    }
}
#Acceder a un diccionario hijo
estadio = supermercado["estadio"]
print(estadio)
#print( supermercado["estadio"]["aseo"] + supermercado["belen"]["aseo"] )
#Accedemos a las ventas de los productos de aseo
ventas_aseo = estadio["aseo"]
print(ventas_aseo)
#Acceder a los trabajadores
trabajadores = estadio["trabajadores"]
print( trabajadores )

"""
Calcular el promedio de ventas de los artículos de cada sede
y anexar una nueva clave en cada diccionario donde tendrá como
valor el promedio de ventas.
Clave -> promedio_ventas
"""

