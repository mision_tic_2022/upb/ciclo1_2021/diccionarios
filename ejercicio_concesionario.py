

#Función para calcular el promedio de ventas de cada trabajador
def promedio_ventas(datos_vendedores: dict):
    #Iterar el diccionario datos_vendedores
    for clave_vendedor, datos in datos_vendedores.items():
        print("--------clave_vendedor-------")
        print(clave_vendedor)
        print("---------ventas---------")
        ventas = datos["ventas"]
        suma_total = 0
        #Iterar lista ventas
        for x in ventas:
            #Obtenemos el valor del diccionario x
            ventas_mes = x.values()
            for item_venta in ventas_mes:
                suma_ventas = 0
                #print(item_venta)
                for marca, venta_marca in item_venta.items():
                    suma_ventas += venta_marca
                    #print("marca: ", marca)
                    #print("ventas: ", venta_marca)
                #print("ventas del mes: ", suma_ventas)
                suma_total += suma_ventas
        #print("Ventas anuales: ", suma_total)
        #Promedio de ventas sumatotal / tamaño de la lista ventas
        promedio_venta_anual = suma_total / len(ventas)
        print("Promedio de venta anual")
        print(promedio_venta_anual)
        #return promedio_venta_anual


"""
Ejercicio 2:
Sacar el promedio de ventas anual de cada vendedor:
"""
datos_vendedores = {
    "Jo0986": {
        "nombre": 'Jorge Alfredo',
        "cedula": '098654',
        "ventas": [
            {
                "enero": {
                    "bmw": 15000000,
                    "jeep": 12400000,
                    "audi": 98500000,
                    "jaguar": 23300000
                }
            },
            {
                "febrero": {
                    "bmw": 1434000,
                    "jeep": 1200000,
                    "audi": 7000000,
                    "jaguar": 9000000
                }
            },
            {
                "marzo": {
                    "bmw": 13000000,
                    "jeep": 12400000,
                    "audi": 1600000,
                    "jaguar": 1800000
                }
            },
            {
                "abril": {
                    "bmw": 12900000,
                    "jeep": 22450000,
                    "audi": 31400000,
                    "jaguar": 1300000
                }
            },
            {
                "mayo": {
                    "bmw": 21800000,
                    "jeep": 632750000,
                    "audi": 81200000,
                    "jaguar": 7200000
                }
            },
            {
                "junio": {
                    "bmw": 10700000,
                    "jeep": 22000000,
                    "audi": 19300000,
                    "jaguar": 5200000
                }
            },
            {
                "julio": {
                    "bmw": 16200000,
                    "jeep": 27180000,
                    "audi": 15300000,
                    "jaguar": 7040000
                }
            },
            {
                "agosto": {
                    "bmw": 15400000,
                    "jeep": 22050000,
                    "audi": 11900000,
                    "jaguar": 2800000
                }
            },
            {
                "septiembre": {
                    "bmw": 18100000,
                    "jeep": 29540000,
                    "audi": 81200000,
                    "jaguar": 6400000
                }
            },
            {
                "octubre": {
                    "bmw": 61400000,
                    "jeep": 52120000,
                    "audi": 61400000,
                    "jaguar": 7500000
                }
            },
            {
                "noviembre": {
                    "bmw": 41300000,
                    "jeep": 52230000,
                    "audi": 31700000,
                    "jaguar": 4200000
                }
            },
            {
                "diciembre": {
                    "bmw": 41000000,
                    "jeep": 52480000,
                    "audi": 125000000,
                    "jaguar": 1000000
                }
            }
        ]
    },
    "Sm2345": {
        "nombre": 'Smith',
        "cedula": '234513',
        "ventas": [
            {
                "enero": {
                    "bmw": 1980000,
                    "jeep": 24200000,
                    "audi": 3940000,
                    "jaguar": 4500000
                }
            },
            {
                "febrero": {
                    "bmw": 4910000,
                    "jeep": 36400000,
                    "audi": 3140000,
                    "jaguar": 4280000
                }
            },
            {
                "marzo": {
                    "bmw": 1920000,
                    "jeep": 43300000,
                    "audi": 2440000,
                    "jaguar": 4210000
                }
            },
            {
                "abril": {
                    "bmw": 9480000,
                    "jeep": 45200000,
                    "audi": 5640000,
                    "jaguar": 4502000
                }
            },
            {
                "mayo": {
                    "bmw": 9620000,
                    "jeep": 45700000,
                    "audi": 5440000,
                    "jaguar": 9380000
                }
            },
            {
                "junio": {
                    "bmw": 9800000,
                    "jeep": 49600000,
                    "audi": 6750000,
                    "jaguar": 2890000
                }
            },
            {
                "julio": {
                    "bmw": 8590000,
                    "jeep": 46200000,
                    "audi": 1940000,
                    "jaguar": 7520000
                }
            },
            {
                "agosto": {
                    "bmw": 1350000,
                    "jeep": 96200000,
                    "audi": 9640000,
                    "jaguar": 8570000
                }
            },
            {
                "septiembre": {
                    "bmw": 2140000,
                    "jeep": 55400000,
                    "audi": 1380000,
                    "jaguar": 9480000
                }
            },
            {
                "octubre": {
                    "bmw": 2550000,
                    "jeep": 34400000,
                    "audi": 1430000,
                    "jaguar": 4450000
                }
            },
            {
                "noviembre": {
                    "bmw": 4350000,
                    "jeep": 13500000,
                    "audi": 25400000,
                    "jaguar": 4520000
                }
            },
            {
                "diciembre": {
                    "bmw": 5260000,
                    "jeep": 83900000,
                    "audi": 7420000,
                    "jaguar": 3400000
                }
            }
        ]
    }
}

promedio_ventas(datos_vendedores)